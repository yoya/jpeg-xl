# Copyright (c) the JPEG XL Project
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.

# We define only two stages for development. The "build" stage only compiles the
# code and doesn't run any target code. The "test" stage runs the code compiled
# by the previous stage. Stages form dependencies in a block, all the build
# targets in one stage need to be completed before any of the next stage can
# start.
stages:
  - build
  - test
  - deploy

variables:
  # We need to fetch the third_party dependencies together with the code unless
  # otherwise noted.
  GIT_SUBMODULE_STRATEGY: recursive

# A template for running in the generic cloud builders. These are tagged with
# "linux" and run on shared VMs.
.linux_builder_template: &linux_builder_template
  image: &jpegxl-builder gcr.io/jpegxl/jpegxl-builder@sha256:b4744eed15a00e88d6faf54798c8d8bca8b58a581bfdda374cfa8c668ec6c1b2
  tags:
    - linux
  # By default all the workflows run on master and on request. This can be
  # override by users of this template.
  only:
    - master
    - tags
    - schedules
  # Retry on system failures. This can be typically caused by the runner or VM
  # being evicted.
  retry:
    max: 2
    when:
      - unknown_failure
      - api_failure
      - stuck_or_timeout_failure
      - runner_system_failure

# A template for building on x86_64 images on the cloud builders.
.linux_x86_64_template: &linux_x86_64_template
  <<: *linux_builder_template
  variables:
    CC: clang-6.0
    CXX: clang++-6.0

.linux_i686_template: &linux_i686_template
  <<: *linux_builder_template
  variables:
    CC: clang-6.0
    CXX: clang++-6.0
    # This makes cross-compiling for i686 the default when calling ci.sh.
    BUILD_TARGET: i686-linux-gnu
    BUILD_DIR: build

# A template for building on aarch64 images on the cloud builders. These may not
# run on actual aarch64 hardware.
.linux_aarch64_template: &linux_aarch64_template
  <<: *linux_builder_template
  variables:
    CC: clang-6.0
    CXX: clang++-6.0
    # This makes cross-compiling for aarch64 the default when calling ci.sh.
    BUILD_TARGET: aarch64-linux-gnu
    BUILD_DIR: build
    # We need to fetch the third_party dependencies together with the code.
    GIT_SUBMODULE_STRATEGY: recursive

# A template for cross-compiling for arm-linux-gnueabihf.
.linux_armhf_template: &linux_armhf_template
  <<: *linux_builder_template
  variables:
    CC: clang-6.0
    CXX: clang++-6.0
    # This makes cross-compiling for armhf the default when calling ci.sh.
    BUILD_TARGET: arm-linux-gnueabihf
    BUILD_DIR: build
    # We need to fetch the third_party dependencies together with the code.
    GIT_SUBMODULE_STRATEGY: recursive

# Common artifacts for the build stage
.default_build_paths: &default_build_paths
  - build/libjpegxl.so*
  - build/tools/cjpegxl
  - build/tools/djpegxl
  - build/tools/benchmark_xl
  - build/tools/decode_and_encode
  - build/tools/comparison_viewer/compare_codecs
  - build/tools/comparison_viewer/compare_images
  - build/tools/viewer/viewer
  # All the test binaries in the build/ directory and .cmake files used by
  # ctest, compressed into this file together to save space when uploading.
  - build/tests.tar.xz
  # The coverage data produced at compile time.
  - build/gcno.tar.xz

.linux_x86_64_build_template: &linux_x86_64_build_template
  <<: *linux_x86_64_template
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 week
    paths: *default_build_paths

.linux_i686_build_template: &linux_i686_build_template
  <<: *linux_i686_template
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 week
    paths: *default_build_paths

.linux_aarch64_build_template: &linux_aarch64_build_template
  <<: *linux_aarch64_template
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 week
    paths: *default_build_paths

.linux_armhf_build_template: &linux_armhf_build_template
  <<: *linux_armhf_template
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 week
    paths: *default_build_paths

# The default Release build runs always.
build:x86_64:clang:release:
  <<: *linux_x86_64_build_template
  stage: build
  only:
    - master
    - merge_requests
    - schedules
    - tags
  script:
    - SKIP_TEST=1 PACK_TEST=1 ./ci.sh release -DCMAKE_INSTALL_PREFIX=`pwd`/prefix
    # Test that the package can be installed.
    - ninja -C build install

test:x86_64:clang:release:
  <<: *linux_x86_64_template
  stage: test
  only:
    - master
    - merge_requests
    - schedules
    - tags
  dependencies:
    - build:x86_64:clang:release
  script:
    - ./ci.sh test
  variables:
    GIT_SUBMODULE_STRATEGY: none

# x86_84 test coverage build
build:x86_64:clang:coverage:
  <<: *linux_x86_64_build_template
  stage: build
  only:
    - master
    - schedules
    - tags
  script:
    - SKIP_TEST=1 PACK_TEST=1 ./ci.sh coverage

test:x86_64:clang:coverage:
  <<: *linux_x86_64_template
  stage: test
  only:
    - master
    - schedules
    - tags
  dependencies:
    - build:x86_64:clang:coverage
  script:
    - ./ci.sh test
    - ./ci.sh coverage_report
  variables:
    CC: clang-6.0
    CXX: clang++-6.0
    # Headers from submodules might be needed when generating the HTML reports.
    GIT_SUBMODULE_STRATEGY: recursive
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 year
    paths:
      - build/coverage.txt
      - build/coverage*.html
      - build/coverage.xml

# Updates the "Pages" section in GitLab with the latest coverage report from
# master.
pages:
  <<: *linux_x86_64_template
  stage: deploy
  only:
    - master
  dependencies:
    - test:x86_64:clang:coverage
  script:
    - mkdir public
    - mv build/coverage* public/
    - mv public/coverage.html public/index.html
    - sed -E "s,GCC Code Coverage Report,Coverage Report for <a href='${CI_PROJECT_URL}/commit/${CI_COMMIT_SHA}'>${CI_COMMIT_SHORT_SHA}</a>," -i public/index.html
  artifacts:
    paths:
      - public


# x86_64 Scalar-only release runs only on master and on request.
build:x86_64:clang:scalar:
  <<: *linux_x86_64_build_template
  stage: build
  only:
    - master
    - merge_requests
    - schedules
    - tags
  script:
    - SKIP_TEST=1 PACK_TEST=1 CMAKE_CXX_FLAGS="-DHWY_STATIC_TARGETS=0 -DHWY_RUNTIME_TARGETS=0" ./ci.sh release

test:x86_64:clang:scalar:
  <<: *linux_x86_64_template
  stage: test
  dependencies:
    - build:x86_64:clang:scalar
  script:
    - ./ci.sh test
  variables:
    GIT_SUBMODULE_STRATEGY: none

# i686 (x86 32-bit) builders
build:i686:clang:release:
  <<: *linux_i686_build_template
  stage: build
  only:
    - master
    - merge_requests
    - schedules
    - tags
  script:
    # AVX2 on 32-bit binaries hits clang compiler bugs.
    - CMAKE_FLAGS="-DHWY_DISABLE_AVX2" SKIP_TEST=1 PACK_TEST=1
        ./ci.sh release -DJPEGXL_ENABLE_TCMALLOC=OFF -DJPEGXL_ENABLE_FUZZERS=OFF

test:i686:clang:release:
  <<: *linux_i686_template
  stage: test
  dependencies:
    - build:i686:clang:release
  script:
    - ./ci.sh test
  variables:
    GIT_SUBMODULE_STRATEGY: none

# aarch64 release runs only on master and on request.
build:aarch64:clang:release:
  <<: *linux_aarch64_build_template
  stage: build
  only:
    - master
    - merge_requests
    - schedules
    - tags
    - web
  script:
    - CMAKE_CXX_FLAGS="-DJXL_DISABLE_SLOW_TESTS" SKIP_TEST=1 PACK_TEST=1
        ./ci.sh release

test:aarch64:clang:release:
  <<: *linux_aarch64_template
  stage: test
  dependencies:
    - build:aarch64:clang:release
  script:
    # LeakSanitizer doesn't work when running under qemu-arm.
    - ASAN_OPTIONS=detect_leaks=0 ./ci.sh test

# arm release
build:armhf:clang:release:
  <<: *linux_armhf_build_template
  stage: build
  only:
    - master
    - merge_requests
    - schedules
    - tags
    - web
  script:
    - CMAKE_CXX_FLAGS="-DJXL_DISABLE_SLOW_TESTS" SKIP_TEST=1 PACK_TEST=1
        ./ci.sh release

test:armhf:clang:release:
  <<: *linux_armhf_template
  stage: test
  dependencies:
    - build:armhf:clang:release
  script:
    - ./ci.sh test

# arm debug
build:armhf:clang:debug:
  <<: *linux_armhf_build_template
  stage: build
  only:
    - merge_requests
  script:
    - SKIP_TEST=1 ./ci.sh debug

# "debug" build enabled for merge_requests. This is here to check that
# debug-only statements still compile.
build:x86_64:clang:debug:
  <<: *linux_x86_64_build_template
  stage: build
  only:
    - merge_requests
  script:
    - SKIP_TEST=1 ./ci.sh debug

# "asan", "tsan" and "msan" builds only run on master, tags and when requested
# from the web. Only supported in x86_64 for now.
build:x86_64:clang:asan:
  <<: *linux_x86_64_build_template
  stage: build
  script:
    - SKIP_TEST=1 PACK_TEST=1 ./ci.sh asan

test:x86_64:clang:asan:
  <<: *linux_x86_64_template
  stage: test
  dependencies:
    - build:x86_64:clang:asan
  script:
    - ./ci.sh test
  variables:
    GIT_SUBMODULE_STRATEGY: none

# aarch64 asan build and test.
build:aarch64:clang:asan:
  <<: *linux_aarch64_build_template
  stage: build
  script:
    - SKIP_TEST=1 PACK_TEST=1 ./ci.sh asan

test:aarch64:clang:asan:
  <<: *linux_aarch64_template
  stage: test
  dependencies:
    - build:aarch64:clang:asan
  # Disable asan test on arm since they timeout.
  only:
    - tags
  script:
    - ./ci.sh test

build:x86_64:clang:tsan:
  <<: *linux_x86_64_build_template
  stage: build
  script:
    - SKIP_TEST=1 PACK_TEST=1 ./ci.sh tsan

test:x86_64:clang:tsan:
  <<: *linux_x86_64_template
  stage: test
  dependencies:
    - build:x86_64:clang:tsan
  script:
    - ./ci.sh test
  variables:
    GIT_SUBMODULE_STRATEGY: none

build:x86_64:clang:msan:
  <<: *linux_x86_64_build_template
  stage: build
  script:
    - SKIP_TEST=1 PACK_TEST=1 ./ci.sh msan

test:x86_64:clang:msan:
  <<: *linux_x86_64_template
  stage: test
  dependencies:
    - build:x86_64:clang:msan
  script:
    - ./ci.sh test
  variables:
    GIT_SUBMODULE_STRATEGY: none

# Merge request linter checks.
build:lint:
  <<: *linux_x86_64_template
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: none
  only:
    - merge_requests
  script:
    - LINT_OUTPUT=clang-format.patch ./ci.sh lint
  allow_failure: true
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 month
    when: on_failure
    paths:
      - clang-format.patch

# Most clang-tidy checks are currently informational only since these are
# currently broken, however several are marked as errors.
build:tidy:
  <<: *linux_x86_64_template
  stage: build
  only:
    - merge_requests
  script:
    - CMAKE_BUILD_TYPE="Release" ./ci.sh tidy
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 month
    when: always
    paths:
      - build/clang-tidy.txt

# Faster benchmark over a smaller set of images.
.benchmark_x86_64_template: &benchmark_x86_64_template
  <<: *linux_x86_64_template
  stage: test
  variables:
    GIT_SUBMODULE_STRATEGY: none
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 year
    paths:
      - build/benchmark_results/

test:fast_benchmark:release:
  <<: *benchmark_x86_64_template
  only:
    - master
    - merge_requests
    - schedules
    - tags
  dependencies:
    - build:x86_64:clang:release
  script:
    - STORE_IMAGES=0 ./ci.sh fast_benchmark

test:fast_benchmark:asan:
  <<: *benchmark_x86_64_template
  dependencies:
    - build:x86_64:clang:asan
  script:
    - STORE_IMAGES=0 ./ci.sh fast_benchmark

test:fast_benchmark.msan:
  <<: *benchmark_x86_64_template
  dependencies:
    - build:x86_64:clang:msan
  script:
    - STORE_IMAGES=0 ./ci.sh fast_benchmark

# This template runs on actual aarch64 hardware.
.benchmark_aarch64_template: &benchmark_aarch64_template
  <<: *linux_builder_template
  image: gcr.io/jpegxl/jpegxl-builder-run-aarch64@sha256:4ff7fe5e3e1d19a8c4604b72a41916173d83ec95d335f7fabd599ad92c438df2
  stage: test
  tags:
    - aarch64
  variables:
    GIT_SUBMODULE_STRATEGY: none
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 year
    paths:
      - build/benchmark_results/

test:aarch64:fast_benchmark:release:
  <<: *benchmark_aarch64_template
  dependencies:
    - build:aarch64:clang:release
  script:
    # Bind to the big CPUs only.
    - ./ci.sh cpuset "${RUNNER_CPU_BIG}"
    - STORE_IMAGES=0 ./ci.sh fast_benchmark

# Benchmark test that runs separately on the big and little CPUs of the runner.
test:aarch64:arm_benchmark:release:
  <<: *benchmark_aarch64_template
  only:
    - web
    - master
  dependencies:
    - build:aarch64:clang:release
  script:
    - ./ci.sh arm_benchmark

# Benchmark ToT on nightly builds. These are scheduled at midnight UTC.
test:benchmark:release:
  image: *jpegxl-builder
  stage: test
  only:
    - schedules
  variables:
    GIT_SUBMODULE_STRATEGY: none
  dependencies:
    - build:x86_64:clang:release
  script:
    - STORE_IMAGES=0 ./ci.sh benchmark
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 year
    paths:
      - build/benchmark_results/

build:tidy:all:
  <<: *linux_x86_64_template
  stage: build
  only:
    - master
  script:
    - CMAKE_BUILD_TYPE="Release" ./ci.sh tidy all
  allow_failure: true
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 year
    when: always
    paths:
      - build/clang-tidy.txt

# Emscripten (WASM) build.
build:ems:all:
  <<: *linux_x86_64_template
  stage: build
  variables:
    CC: emcc
    CXX: em++
    EMSDK: /opt/emsdk
    EM_CONFIG: /opt/emsdk/.emscripten
    CMAKE_TOOLCHAIN_FILE: /opt/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    expire_in: 1 week
    paths:
      - build/jpegxl_emcc.*
  script:
    - source "${EMSDK}/emsdk_env.sh"
    - SKIP_TEST=1 ./ci.sh release
